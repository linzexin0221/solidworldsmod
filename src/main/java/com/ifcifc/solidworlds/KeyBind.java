package com.ifcifc.solidworlds;

import com.ifcifc.solidworlds.ConfigMenu.SettingMenu;
import net.fabricmc.fabric.api.client.keybinding.FabricKeyBinding;
import net.fabricmc.fabric.api.client.keybinding.KeyBindingRegistry;
import net.fabricmc.fabric.api.event.client.ClientTickCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.util.InputUtil;
import net.minecraft.util.Identifier;
import org.lwjgl.glfw.GLFW;

import java.io.File;

public class KeyBind {
    public static FabricKeyBinding KEYOpenSettings = FabricKeyBinding.Builder.create(new Identifier("solidworlds", "opensettings"), InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_UNKNOWN, "SolidWorlds").build();


    public static void initialize() {
        KeyBindingRegistry.INSTANCE.addCategory("SolidWorlds");
        KeyBindingRegistry.INSTANCE.register(KEYOpenSettings);

        ClientTickCallback.EVENT.register(tick -> {
            if (null == tick.player) return;
            if (KEYOpenSettings.wasPressed()) {
                try {
                    tick.openScreen(SettingMenu.getConfigBuilderScreen(ConfigMOD.options).setSavingRunnable(() -> {
                        ConfigMOD.updateSave(
                                new File(FabricLoader.getInstance().getGameDirectory(), "/saves/" + ConfigMOD.worldName + "/solidWordls.json"),
                                ConfigMOD.options
                        );
                        ConfigMOD.loadWorldConfig();
                    }).transparentBackground().build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
