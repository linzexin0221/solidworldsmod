package com.ifcifc.solidworlds;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.server.ServerTickCallback;

public class MainMod implements ModInitializer {

    public MainMod() {

    }

    public void test(String A, String B) {
        if (A.length() != B.length()) {
            System.out.println("Length: " + A + " - " + B);
            return;
        }
        for (int i = 0; i < A.length(); i++) {
            if (A.charAt(i) != B.charAt(i)) {
                System.out.println(i + ": " + A.charAt(i) + " != " + B.charAt(i));
            }
        }
        if (!A.equals(B)) System.out.println("Not EQUALS: " + A + " - " + B);
    }

    @Override
    public void onInitialize() {
        ConfigMOD.initialize();
        KeyBind.initialize();

        ServerTickCallback.EVENT.register(t -> {
            //if(t.isClient)return;

            try {
                String name = ConfigMOD.getLevelName();
                //test(ConfigMOD.worldName, name);

                if (!ConfigMOD.worldName.equals(name)) {
                    ConfigMOD.worldName = name;
                    //test(ConfigMOD.worldName, name);
                    if (!ConfigMOD.worldName.equals("default")) ConfigMOD.loadWorldConfig();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }
}
