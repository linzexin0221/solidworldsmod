package com.ifcifc.solidworlds.ConfigMenu;

import com.ifcifc.solidworlds.ConfigMOD;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.impl.builders.SubCategoryBuilder;
import net.minecraft.text.TranslatableText;

public class SettingMenu {
    public static ConfigBuilder getConfigBuilderScreen(ConfigMOD.config options) {
        ConfigBuilder builder = ConfigBuilder.create();
        builder.transparentBackground();

        final ConfigMOD.config config = new ConfigMOD.config(1);

        builder.setTitle(new TranslatableText("config.solidworlds.title"));

        ConfigEntryBuilder eb = builder.entryBuilder();
        ConfigCategory general = builder.getOrCreateCategory(new TranslatableText("config.solidworlds.title"));
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.cave"), options.Cave)
                .setDefaultValue(config.Cave)
                .setSaveConsumer(v -> options.Cave = v)
                .build());
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.ravine"), options.Ravine)
                .setDefaultValue(config.Ravine)
                .setSaveConsumer(v -> options.Ravine = v)
                .build());
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.underwaterCave"), options.UnderwaterCave)
                .setDefaultValue(config.UnderwaterCave)
                .setSaveConsumer(v -> options.UnderwaterCave = v)
                .build());
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.underwaterRavine"), options.UnderwaterRavine)
                .setDefaultValue(config.UnderwaterRavine)
                .setSaveConsumer(v -> options.UnderwaterRavine = v)
                .build());
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.lavalake"), options.LavaLake)
                .setDefaultValue(config.LavaLake)
                .setSaveConsumer(v -> options.LavaLake = v)
                .build());
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.waterlake"), options.WaterLake)
                .setDefaultValue(config.WaterLake)
                .setSaveConsumer(v -> options.WaterLake = v)
                .build());
        general.addEntry(eb.startBooleanToggle(new TranslatableText("config.solidworlds.mineshaft"), options.Mineshaft)
                .setDefaultValue(config.Mineshaft)
                .setSaveConsumer(v -> options.Mineshaft = v)
                .build());

        final SubCategoryBuilder surfaceLake = eb.startSubCategory(new TranslatableText("config.solidworlds.title_surfacelake")).setExpanded(false);

        surfaceLake.add(eb.startBooleanToggle(new TranslatableText("config.solidworlds.surfacelavalake"), options.SurfaceLavaLake)
                .setDefaultValue(config.SurfaceLavaLake)
                .setSaveConsumer(v -> options.SurfaceLavaLake = v)
                .build());
        surfaceLake.add(eb.startBooleanToggle(new TranslatableText("config.solidworlds.surfacewaterlake"), options.SurfaceWaterLake)
                .setDefaultValue(config.SurfaceWaterLake)
                .setSaveConsumer(v -> options.SurfaceWaterLake = v)
                .build());

        surfaceLake.add(eb.startLongSlider(new TranslatableText("config.solidworlds.surfacelavalake_chance"), options.SurfaceLavaLakeChance, 1, 1024)
                .setDefaultValue(config.SurfaceLavaLakeChance)
                .setSaveConsumer(v -> options.SurfaceLavaLakeChance = Math.toIntExact(v))
                .build());
        surfaceLake.add(eb.startLongSlider(new TranslatableText("config.solidworlds.surfacewaterlake_chance"), options.SurfaceWaterLakeChance, 1, 1024)
                .setDefaultValue(config.SurfaceWaterLakeChance)
                .setSaveConsumer(v -> options.SurfaceWaterLakeChance = Math.toIntExact(v))
                .build());

        surfaceLake.add(eb.startLongSlider(new TranslatableText("config.solidworlds.surfacelavalake_height"), options.SurfaceLavaLakeHeight, 1, 255)
                .setDefaultValue(config.SurfaceLavaLakeHeight)
                .setSaveConsumer(v -> options.SurfaceLavaLakeHeight = Math.toIntExact(v))
                .build());
        surfaceLake.add(eb.startLongSlider(new TranslatableText("config.solidworlds.surfacewaterlake_height"), options.SurfaceWaterLakeHeight, 1, 255)
                .setDefaultValue(config.SurfaceWaterLakeHeight)
                .setSaveConsumer(v -> options.SurfaceWaterLakeHeight = Math.toIntExact(v))
                .build());

        surfaceLake.add(eb.startLongSlider(new TranslatableText("config.solidworlds.surfacelavalake_miny"), options.SurfaceLavaLakeMinY, 1, 255)
                .setDefaultValue(config.SurfaceLavaLakeMinY)
                .setSaveConsumer(v -> options.SurfaceLavaLakeMinY = Math.toIntExact(v))
                .build());
        surfaceLake.add(eb.startLongSlider(new TranslatableText("config.solidworlds.surfacewaterlake_miny"), options.SurfaceWaterLakeMinY, 1, 255)
                .setDefaultValue(config.SurfaceWaterLakeMinY)
                .setSaveConsumer(v -> options.SurfaceWaterLakeMinY = Math.toIntExact(v))
                .build());

        general.addEntry(surfaceLake.build());

        return builder;
    }
}
