package com.ifcifc.solidworlds.mixin;


import com.ifcifc.solidworlds.ConfigMOD;
import com.ifcifc.solidworlds.SurfaceLakes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.decorator.ChanceDecoratorConfig;
import net.minecraft.world.gen.decorator.DecoratorContext;
import net.minecraft.world.gen.decorator.LavaLakeDecorator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Random;
import java.util.stream.Stream;

@Mixin(LavaLakeDecorator.class)
public class LavaLakeDecoratorMixin {
    @Inject(method = "getPositions",
            cancellable = true,
            at = @At("HEAD"))
    public void getPositions(DecoratorContext decoratorContext, Random random, ChanceDecoratorConfig chanceDecoratorConfig, BlockPos blockPos, CallbackInfoReturnable<Stream<BlockPos>> cir){
        if (ConfigMOD.options.SurfaceLavaLake) {
            cir.setReturnValue(SurfaceLakes.generateLake(
                    random,
                    blockPos,
                    ConfigMOD.options.SurfaceLavaLakeChance,
                    ConfigMOD.options.SurfaceLavaLakeMinY,
                    ConfigMOD.options.SurfaceLavaLakeHeight
            ));
        } else if (!ConfigMOD.options.LavaLake) cir.setReturnValue(Stream.empty());
    }
}
