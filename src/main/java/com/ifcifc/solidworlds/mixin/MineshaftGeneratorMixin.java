package com.ifcifc.solidworlds.mixin;


import com.ifcifc.solidworlds.ConfigMOD;
import net.minecraft.structure.MineshaftGenerator;
import net.minecraft.structure.StructurePiece;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.StructureAccessor;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;
import java.util.Random;

@Mixin(MineshaftGenerator.MineshaftRoom.class)
public class MineshaftGeneratorMixin {

    @Inject(method = "fillOpenings",
            cancellable = true,
            at = @At("HEAD"))
    public void placeJigsaw(StructurePiece structurePiece, List<StructurePiece> list, Random random, CallbackInfo ci) {
        if (!ConfigMOD.options.Mineshaft) ci.cancel();
    }

    @Inject(method = "generate",
            cancellable = true,
            at = @At("HEAD"))

    public void generate(StructureWorldAccess structureWorldAccess, StructureAccessor structureAccessor, ChunkGenerator chunkGenerator, Random random, BlockBox boundingBox, ChunkPos chunkPos, BlockPos blockPos, CallbackInfoReturnable<Boolean> cir) {
        if (!ConfigMOD.options.Mineshaft) cir.setReturnValue(false);

    }
}
